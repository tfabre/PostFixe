#ifndef __MACRO_OPERATOR_C_H
#define __MACRO_OPERATOR_C_H

#include <stdarg.h>

#include "operator_creator_list.h"
#include "smem.h"
#include "value_error.h"
#include "linked_list_chunk.h"

#undef OPERATOR_DECLARE

typedef long long int lli_t;
typedef long double   ld_t;

#define OPERATOR_DECLARE(type_name)                         \
    chunk operator_ ## type_name ## _create()               \
    {                                                       \
        return NULL;                                        \
    }                                                       \
    bool operator_is_ ## type_name(const chunk ch)          \
    {                                                       \
        return true;                                        \
    }

/*
 * Except for operator label, all are identifcal.
 * This is simplified into making only one static instance of the operator for
 * each kind.
 * Creation and destruction are thus handled regularly as any global variable.
 */

#define OPERATOR_BASIC_FULL(op_name, op)                                \
    static basic_type operator_ ## op_name ## _print(const chunk ch,    \
                                                     va_list va)        \
    {                                                                   \
        FILE* out = va_arg(va, FILE*);                                  \
        for (int i = 0; NULL != operator_creator_list[i].keyword &&     \
                 NULL != operator_creator_list[i].create_operator; ++i) \
            if (operator_creator_list[i].create_operator ==             \
                &operator_ ## op_name ## _create)                       \
                fprintf(out, "%s", operator_creator_list[i].keyword);   \
                                                                        \
        return basic_type_void;                                         \
    }                                                                   \
                                                                        \
    static basic_type operator_ ## op_name ## _destroy(const chunk ch,  \
                                                       va_list va)      \
    {                                                                   \
        return basic_type_error;                                        \
    }                                                                   \
                                                                        \
    static basic_type operator_ ## op_name ## _copy(const chunk ch,     \
                                                    va_list va)         \
    {                                                                   \
        return basic_type_pointer((void*)operator_ ## op_name ## _create()); \
    }                                                                   \
                                                                        \
    static const message_action operator_ ## op_name ## _reactions[] =  \
    {                                                                   \
        MESSAGE_ACTION__BASIC_OPERATOR(op_name),                        \
        { NULL, NULL }                                                  \
    };                                                                  \
                                                                        \
    static chunk_struct operator_ ## op_name ## _instance =             \
    {                                                                   \
        .state = NULL,                                                  \
        .reactions = operator_ ## op_name ## _reactions                 \
    };                                                                  \
                                                                        \
    chunk operator_ ## op_name ## _create()                             \
    {                                                                   \
        return &operator_ ## op_name ## _instance;                      \
    }                                                                   \
                                                                        \
    bool operator_is_ ## op_name(const chunk ch)                        \
    {                                                                   \
        assert(NULL != ch);                                             \
        return ch->reactions == operator_ ## op_name ## _reactions;     \
    }

#define OPERATOR_NUMBER(op_name, op)                                    \
    static basic_type operator_ ## op_name ## _evaluate(const chunk ch, \
                                                        va_list va)     \
    {                                                                   \
        interpretation_context context = va_arg(va, interpretation_context); \
        chunk num1 = linked_list_chunk_get_front(context->stack);       \
                                                                        \
        if (NULL == num1 || (!value_is_int(num1) && !value_is_double(num1))) \
            return basic_type_error;                                    \
                                                                        \
        linked_list_chunk_pop_front(context->stack);                    \
        chunk num2 = linked_list_chunk_get_front(context->stack);       \
                                                                        \
        if (NULL == num2 || (!value_is_int(num2) && !value_is_double(num2))) \
        {                                                               \
            linked_list_chunk_add_front(context->stack, num1);          \
            return basic_type_error;                                    \
        }                                                               \
                                                                        \
        linked_list_chunk_pop_front(context->stack);                    \
                                                                        \
        chunk ret = NULL;                                               \
        if (value_is_int(num1) && value_is_int(num2))                   \
            ret = value_int_create(*(lli_t*)num2->state op              \
                                   *(lli_t*)num1->state);               \
        else if (value_is_double(num1) && value_is_double(num2))        \
            ret = value_double_create(*(ld_t*)num2->state op            \
                                      *(ld_t*)num1->state);             \
        else if (value_is_int(num1) && value_is_double(num2))           \
            ret = value_double_create(*(ld_t*)num2->state op            \
                                      *(lli_t*)num1->state);            \
        else                                                            \
            ret = value_double_create(*(lli_t*)num2->state op           \
                                      *(ld_t*)num1->state);             \
        linked_list_chunk_add_front(context->stack, ret);               \
        chunk_destroy(num1);                                            \
        chunk_destroy(num2);                                            \
        return basic_type_void;                                         \
    }                                                                   \
    OPERATOR_BASIC_FULL(op_name, op)

#define OPERATOR_BOOLEAN(op_name, op)                                   \
    static basic_type operator_ ## op_name ## _evaluate(const chunk ch, \
                                                       va_list va)      \
    {                                                                   \
        interpretation_context context = va_arg(va, interpretation_context); \
        chunk bool1 = linked_list_chunk_get_front(context->stack);      \
                                                                        \
        if (NULL == bool1 || !value_is_boolean(bool1))                  \
            return basic_type_error;                                    \
                                                                        \
        linked_list_chunk_pop_front(context->stack);                    \
        chunk bool2 = linked_list_chunk_get_front(context->stack);      \
        if (NULL == bool2 || !value_is_boolean(bool2))                  \
        {                                                               \
            linked_list_chunk_add_front(context->stack, bool1);         \
            return basic_type_error;                                    \
        }                                                               \
        linked_list_chunk_pop_front(context->stack);                    \
                                                                        \
        chunk ret = value_boolean_create(*(bool*)bool2->state op        \
                                         *(bool*)bool1->state);         \
        linked_list_chunk_add_front(context->stack, ret);               \
        chunk_destroy(bool1);                                           \
        chunk_destroy(bool2);                                           \
        return basic_type_void;                                         \
    }                                                                   \
    OPERATOR_BASIC_FULL(op_name, op)
    
#define OPERATOR_COMPARATOR(op_name, op)                                \
    static basic_type operator_ ## op_name ## _evaluate(const chunk ch, \
                                                        va_list va)     \
    {                                                                   \
        interpretation_context context = va_arg(va, interpretation_context); \
        chunk val2 = linked_list_chunk_get_front(context->stack);       \
                                                                        \
        if (NULL == val2)                                               \
            return basic_type_error;                                    \
                                                                        \
        linked_list_chunk_pop_front(context->stack);                    \
        chunk val1 = linked_list_chunk_get_front(context->stack);       \
        if (NULL == val1)                                               \
        {                                                               \
            linked_list_chunk_add_front(context->stack, val2);          \
            return basic_type_error;                                    \
        }                                                               \
        linked_list_chunk_pop_front(context->stack);                    \
                                                                        \
        bool result;                                                    \
        if (value_is_int(val1) && value_is_int(val2))                   \
            result = *(lli_t*)val1->state op *(lli_t*)val2->state;      \
        else if (value_is_int(val1) && value_is_double(val2))           \
            result = (ld_t)*(lli_t*)val1->state op *(ld_t*)val2->state; \
        else if (value_is_double(val1) && value_is_int(val2))           \
            result = *(ld_t*)val1->state op (ld_t)*(lli_t*)val2->state; \
        else if (value_is_double(val1) && value_is_double(val2))        \
            result = *(ld_t*)val1->state op *(ld_t*)val2->state;        \
        else if (value_is_sstring(val1) && value_is_sstring(val2))      \
            result = sstring_compare(*(sstring*)val1->state,            \
                                     *(sstring*)val2->state) op 0;      \
        else                                                            \
        {                                                               \
            linked_list_chunk_add_front(context->stack, val2);          \
            linked_list_chunk_add_front(context->stack, val1);          \
            return basic_type_error;                                    \
        }                                                               \
        chunk ret = value_boolean_create(result);                       \
        linked_list_chunk_add_front(context->stack, ret);               \
        chunk_destroy(val1);                                            \
        chunk_destroy(val2);                                            \
        return basic_type_void;                                         \
    }                                                                   \
    OPERATOR_BASIC_FULL(op_name, op)

#define OPERATOR_EQUALITY(op_name, op)                                  \
    static basic_type operator_ ## op_name ## _evaluate(const chunk ch, \
                                                        va_list va)     \
    {                                                                   \
        interpretation_context context = va_arg(va, interpretation_context); \
        chunk val1 = linked_list_chunk_get_front(context->stack);       \
                                                                        \
        if (NULL == val1)                                               \
            return basic_type_error;                                    \
                                                                        \
        linked_list_chunk_pop_front(context->stack);                    \
        chunk val2 = linked_list_chunk_get_front(context->stack);       \
        if (NULL == val2)                                               \
        {                                                               \
            linked_list_chunk_add_front(context->stack, val1);          \
            return basic_type_error;                                    \
        }                                                               \
        linked_list_chunk_pop_front(context->stack);                    \
                                                                        \
        bool result;                                                    \
        if (value_is_int(val1) && value_is_int(val2))                   \
            result = *(lli_t*)val1->state op *(lli_t*)val2->state;      \
        else if (value_is_int(val1) && value_is_double(val2))           \
            result = (ld_t)*(lli_t*)val1->state op *(ld_t*)val2->state; \
        else if (value_is_double(val1) && value_is_int(val2))           \
            result = *(ld_t*)val1->state op (ld_t)*(lli_t*)val2->state; \
        else if (value_is_double(val1) && value_is_double(val2))        \
            result = *(ld_t*)val1->state op *(ld_t*)val2->state;        \
        else if (value_is_sstring(val1) && value_is_sstring(val2))      \
            result = sstring_compare(*(sstring*)val1->state,            \
                                     *(sstring*)val2->state) op 0;      \
        else if (value_is_boolean(val1) && value_is_boolean(val2))      \
            result = *(bool*)val1->state op *(bool*)val2->state;        \
        else                                                            \
        {                                                               \
            linked_list_chunk_add_front(context->stack, val2);          \
            linked_list_chunk_add_front(context->stack, val1);          \
            return basic_type_error;                                    \
        }                                                               \
        chunk ret = value_boolean_create(result);                       \
        linked_list_chunk_add_front(context->stack, ret);               \
        chunk_destroy(val1);                                            \
        chunk_destroy(val2);                                            \
        return basic_type_void;                                         \
    }                                                                   \
    OPERATOR_BASIC_FULL(op_name, op)

#endif
