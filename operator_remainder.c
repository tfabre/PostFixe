#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_remainder.h"
#include "macro_operator_c.h"
#include "value_int.h"
#include "value_double.h"
#include "value_error.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c %: replace the two \c value_int's on top of the stack by
 *        the remainder of their integer division.
 *
 * The \c value's must be \c value_int.
 * Result is a \c value_int.
 * The \c value_int's are destroyed.
 * 
 * If the stack is not deep enough or a wrong kind of \c value is found, then a
 * \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static basic_type operator_remainder_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk num1 = linked_list_chunk_get_front(context->stack);

    if (NULL == num1 || !value_is_int(num1))
        return basic_type_error;

    linked_list_chunk_pop_front(context->stack);
    chunk num2 = linked_list_chunk_get_front(context->stack);

    if (NULL == num2 || !value_is_int(num2))
    {
        linked_list_chunk_add_front(context->stack, num1);
        return basic_type_error;
    }

    linked_list_chunk_pop_front(context->stack);

    chunk ret = value_int_create(*(long long int*)num2->state %
                                 *(long long int*)num1->state);

    linked_list_chunk_add_front(context->stack, ret);
    chunk_destroy(num1);
    chunk_destroy(num2);
    return basic_type_void;
}

OPERATOR_BASIC_FULL(remainder, remainder)
