#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_not.h"
#include "macro_operator_c.h"
#include "value.h"
#include "value_boolean.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c !: replace the \c value_boolean on top of the stack by
 *        its negation.
 *
 * The \c value_boolean is destroyed.
 * 
 * If the stack is not deep enough or a wrong kind of \c value is found, then a
 * \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static basic_type operator_not_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk boolean = linked_list_chunk_get_front(context->stack);

    if (NULL == boolean || !value_is_boolean(boolean))
        return basic_type_error;

    linked_list_chunk_pop_front(context->stack);

    chunk ret = value_boolean_create(!*(bool*)boolean->state);
    linked_list_chunk_add_front(context->stack, ret);
    chunk_destroy(boolean);
    return basic_type_void;
}

OPERATOR_BASIC_FULL(not, !)
