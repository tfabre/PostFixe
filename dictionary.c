#include <stdlib.h> // malloc + free
#include <assert.h>

#include "dictionary.h"
#include "smem.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Provide a dictionary(associative table).
 * \li keys must be non-empty \c sstring's
 * \li values must be \c chunk's
 *
 * Keys and values are copied to get independent long term storage.
 * In the same spirit, queried values are \b copies. 
 * The <b>caller is in charge of destroying copies</b>; this is indicated by the
 * \c _copy in the query function name.
 *
 * Values are stored in an ordered binary tree.
 * They are two data structures: one for the dictionary and one for nodes.
 *
 * \note Cela ressemble au TDM 3, même s'il y a des différences.
 *
 * assert is enforced.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

/*! \c dictionary is a pointer to the hidden structure for dictionary.*/
typedef struct node_s
{
    sstring key;
    chunk val;
} node_t;

static node_t* node_init(sstring key, chunk val)
{
    node_t* ret = (node_t*)malloc(sizeof(node_t));
    ret->key = sstring_copy(key);
    ret->val = chunk_copy(val);
    return ret;
}

static void node_destroy(node_t* node)
{
    assert(NULL != node);
    sstring_destroy(node->key);
    chunk_destroy(node->val);
    sfree(node);
}

static void node_print(const node_t* node, FILE* stream)
{
    fputc('"', stream);
    sstring_print(node->key, stream);
    fputs("\" => ", stream);
    chunk_print(node->val, stream);
    fputc('\n', stream);
}

struct dictionary_struct
{
    node_t* node;
    dictionary parent;
    dictionary lson;
    dictionary rson;
};

/*!
 * Generate an empty \c dictionary.
 *
 * \return an empty \c dictionary
 */
dictionary dictionary_create(void)
{
    dictionary ret = (dictionary)smalloc(sizeof(struct dictionary_struct));

    ret->node = NULL;
    ret->parent = NULL;
    ret->lson = NULL;
    ret->rson = NULL;

    return ret;
}

static void dictionary_insert_leaf(dictionary leaf, sstring key, chunk val,
                                   int comp)
{
    if (0 == comp)
    {
        chunk_destroy(leaf->node->val);
        leaf->node->val = chunk_copy(val);
        return;
    }

    dictionary new_leaf = dictionary_create();
    new_leaf->parent = leaf;

    new_leaf->node = node_init(key, val);

    if (1 <= comp)
        leaf->lson = new_leaf;
    else
        leaf->rson = new_leaf;
}

void insert_leaf(dictionary son, sstring key, chunk val, dictionary dic,
                 const int cmp)
{
    if (NULL != son)
        dictionary_set(son, key, val);
    else
        dictionary_insert_leaf(dic, key, val, cmp);
}

/*!
 * Add an entry \c(key,val) into a \c dictionary.
 *
 * Copies of key and val are made.
 * The original values can be safely destroyed after the call.
 *
 * \param dic \c dictionary to modify
 * \param key insertion key
 * \param val inserted value
 * \pre no pointer is NULL(assert-ed)
 * \pre key is not an empty string (assert-ed)
 */
void dictionary_set(dictionary dic, sstring key, chunk val)
{
    assert(NULL != dic);
    assert(NULL != key);
    assert(NULL != val);

    if (NULL == dic->node)
    {
        dic->node = node_init(key, val);
        return;
    }

    int cmp = sstring_compare(dic->node->key, key);
    if (cmp == 0)
        dictionary_insert_leaf(dic, key, val, cmp);

    else if (cmp >= 1)
        insert_leaf(dic->lson, key, val, dic, cmp);

    else
        insert_leaf(dic->rson, key, val, dic, cmp);
}

/*!
 * Retrieve a \b copied value from a \c dictionary according to a \c key.
 *
 * \param dic \c dictionary to query from
 * \param key key to search a value for
 * \pre no pointer is NULL(assert-ed)
 * \pre key is not empty
 * \return a \b copy of the associated \c chunk or NULL if undefined 
 */
chunk dictionary_get_copy(dictionary dic, sstring key)
{
    assert(NULL != dic);
    assert(NULL != key);

    dictionary elem = dic;

    while (NULL != elem && NULL != elem->node)
    {
        int cmp = sstring_compare(elem->node->key, key);

        if (cmp == 0)
            return chunk_copy(elem->node->val);

        else if (cmp >= 1)
            elem = elem->lson;

        else
            elem = elem->rson;
    }

    return NULL;
}

static void dictionary_destroy_rec(dictionary* dic)
{
    if ((*dic)->lson != NULL)
        dictionary_destroy((*dic)->lson);
    if ((*dic)->rson != NULL)
        dictionary_destroy((*dic)->rson);

    if (NULL != (*dic)->node)
        node_destroy((*dic)->node);
}

/*!
 * Destroy a \c dictionary and released associated resources.
 * All keys and values are destroyed.
 *
 * \param dic \c dictionary to destroy
 * \pre no pointer is NULL(assert-ed)
 */
void dictionary_destroy(dictionary dic)
{
    assert(NULL != dic);
    dictionary_destroy_rec(&dic);
    sfree(dic);
}

/*!
 * Print a \c dictionary to a stream.
 * Entries are printed in \c key alphabetical order.
 * The format is as in the following example with and \c value_integer and a \c
 * value_blok
 \verbatim
 "Bob" => 3
 "Increment_Bob" =>
 {
 Bob
 1
 +
 \Bob
 def
 } \endverbatim
 * \param dic \c dictionary to print
 * \param f stream to print to
 * \pre no pointer is NULL(assert-ed)
 */
void dictionary_print(dictionary dic, FILE* f)
{
    assert(NULL != dic);

    if (dic->lson != NULL)
        dictionary_print(dic->lson, f);

    if (dic->node != NULL)
        node_print(dic->node, f);

    if (dic->rson != NULL)
        dictionary_print(dic->rson, f);
}
