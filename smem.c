#include "smem.h"

#include <assert.h>
#include <string.h>

#undef NDEBUG

void* smalloc(const size_t size)
{
    void* ret = malloc(size);
    assert(NULL != ret);
    return ret;
}

void* scalloc(const size_t num, const size_t size)
{
    void* ret = calloc(num, size);
    assert(NULL != ret);
    return ret;
}

char* rescale(char** src, const size_t size)
{
    char* c = (char*)scalloc(sizeof(char), size);
    strcpy(c, *src);
    sfree(*src);
    *src = c;
    return *src;
}
