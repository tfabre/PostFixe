#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "smem.h"
#include "sstring.h"

#undef NDEBUG        // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief This module provides a « safer » string.
 * 
 * These are not C-string but a \c struct that holds the length of the string
 * and a pointer to the actual char sequence.
 * Please note that there is no \c '\0' to mark the end of the \c sstring.
 *
 * Empty string is encoded by 0 \c length and \c NULL \c pointer.
 *
 * assert is enforced.
 *
 * \note Cela ressemble au TDM 1, même s'il y a des différences.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

/*! \c sstring is a pointer to a hidden structure. */

struct sstring_struct
{
    unsigned int length;
    char* string;
};

/*!
 * Generate an empty \c sstring.
 *
 * \return an empty \c sstring
 */
sstring sstring_create_empty(void) 
{
    sstring sst = (sstring)smalloc(sizeof(struct sstring_struct));
    sst->length = 0;
    sst->string = NULL; 

    return sst;
}

/*!
 * Generate a \c sstring with the same \c char sequence as a C-string.
 *
 * \param st C-string
 * \pre st is not \c NULL(assert-ed)
 * \return a sstring corresponding to st
 */
sstring sstring_create_string(const char* const st) 
{
    assert(NULL != st);

    sstring sst = (sstring)smalloc(sizeof(struct sstring_struct));
    sst->length = strlen(st);
    sst->string = (char*)smalloc(sizeof(char) * strlen(st));

    memcpy(sst->string, st, sst->length);

    return sst;
}

/*!
 * Destroy a \c sstring and release related resources.
 *
 * \param ss C-string to destroy
 * \pre ss is a valid \c sstring(assert-ed)
 */
void sstring_destroy(sstring ss) 
{
    assert(NULL != ss);
    sfree(ss->string);
    sfree(ss);
}

static void interpret_cntrl(const char cntrl, FILE* f)
{
    switch (cntrl)
    {
    case 'n':
        fputc('\n', f);
        break;
    case 't':
        fputc('\t', f);
        break;
    case '"':
        fputc('"', f);
        break;
    default:
        fputc('?', f);
        break;
    }
}

/*!
 * Print a \c sstring to a stream.
 * There is no line return (unless it is contained in \c ss).
 *
 * \param ss \c sstring to print
 * \param f stream to print to
 * \pre ss is a valid \c sstring(assert-ed)
 * \pre f is not \c NULL(assert-ed)
 */
void sstring_print(sstring ss, FILE* f) 
{
    assert(ss != NULL);
    assert(f != NULL);

    const unsigned int len = ss->length;
    for (unsigned int i = 0; i < len; i++)
    {
        if ('\\' == ss->string[i])
            interpret_cntrl(ss->string[++i], f);
        else
            fprintf(f, "%c", ss->string[i]);
    }
}

/*!
 * Concatenate a \c sstring at the end of another.
 *
 * \param ss1 \c sstring to be modified
 * \param ss2 \c sstring to concatenate to \c ss1
 * \pre \c ss1 and \c ss2 are valid \c sstring(assert-ed)
 */
void sstring_concatenate(sstring ss1, sstring ss2) 
{
    assert(NULL != ss1);
    assert(NULL != ss2);

    const unsigned int ss1_len = ss1->length;
    const unsigned int ss2_len = ss2->length;
    const unsigned int tlen = ss1_len + ss2_len;

    char* s = malloc(tlen * sizeof(char));

    memcpy(s, ss1->string, ss1_len);
    memcpy(s + ss1_len, ss2->string, ss2_len);

    free(ss1->string);
    ss1->string = s;
    ss1->length = tlen;
}


/*!
 * Provide a copy of a string.
 *
 * \param ss \c sstring to copy
 * \pre ss is a valid \c sstring(assert-ed)
 * \return an independant copy of \c ss
 */
sstring sstring_copy(sstring ss) 
{
    assert(NULL != ss);

    sstring ss_cpy = sstring_create_empty();
    ss_cpy->length = ss->length;
    ss_cpy->string = (char*)smalloc(sizeof(char) * ss->length);
    memcpy(ss_cpy->string, ss->string, ss->length);
    return ss_cpy;
}

/*!
 * Indicate how two \c sstring are ordered alphabetically.
 *
 * \param ss1 \c sstring 
 * \param ss2 \c sstring 
 * \pre ss1 and ss2 are valid \c sstring(assert-ed)
 * \return 
 * \li 0 if ss1 == ss2
 * \li -1 if ss1 < ss2
 * \li 1 otherwise
 */
int sstring_compare(sstring ss1, sstring ss2) 
{
    assert(NULL != ss1);
    assert(NULL != ss2);

    const bool diff_len = ss1->length == ss2->length;
    const bool ss1_gt_ss2 = ss1->length > ss2->length;
    const unsigned int len_min = (ss1_gt_ss2)
        ? ss2->length
        : ss1->length;

    for (unsigned int i = 0; i < len_min; ++i)
    {
        if (ss1->string[i] < ss2->string[i])
            return -1;

        else if (ss1->string[i] > ss2->string[i])
            return 1;
    }

    if (!diff_len)
        return (ss1_gt_ss2) ? 1 : -1;

    return 0;
}

/*!
 * Indicate whether a string is empty.
 *
 * This function has no side effect and can be safely used in asserts.
 *
 * \param ss \c sstring to test
 * \pre ss is a valid \c sstring(assert-ed)
 * \return true ssi \c ss is empty
 */
bool sstring_is_empty(sstring ss) 
{
    assert(ss->string != NULL);

    return ss->length == 0;
}
