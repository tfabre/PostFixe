#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "basic_type.h"
#include "operator_if.h"
#include "macro_operator_c.h"
#include "value_boolean.h"
#include "value_block.h"
#include "value_protected_label.h"
#include "value_error.h"
#include "operator_label.h"
#include "sstring.h"
#include "interpreter.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c if: when evaluated depending the \c value_boolean on top
 *        of the stack activates or not the next value on the stack.
 *
 * Both values are destroyed.
 * Value on top must be a \c value_boolean and under should be a \c value_block
 * or \c value_protected_label.
 * If this last \c value is a \c value_block, the chunks evaluated in order.
 * If this last \c value is a \c value_protected_label, then it as evaluated as
 * the corresponding \c operator_label.
 * 
 * If the stack is not deep enough or a wrong kind of \c value is found, then a
 * \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static basic_type operator_if_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk cond = linked_list_chunk_get_front(context->stack);

    if (NULL == cond || !value_is_boolean(cond))
        return basic_type_error;

    linked_list_chunk_pop_front(context->stack);
    chunk op = linked_list_chunk_get_front(context->stack);
    if (NULL == op)
    {
        linked_list_chunk_add_front(context->stack, cond);
        return basic_type_error;
    }
    linked_list_chunk_pop_front(context->stack);

    if (*(bool*)cond->state)
    {
        if (value_is_block(op))
            interprete_chunk_list(value_block_get_list(op), context);

        else if (value_is_protected_label(op))
        {
            sstring label_name = sstring_copy(*(sstring*)op->state);
            interprete_chunk(operator_label_create(label_name), context);
        }
        else
            interprete_chunk(op, context);
    }

    chunk_destroy(cond);
    chunk_destroy(op);
    return basic_type_void;
}

OPERATOR_BASIC_FULL(if, if)
