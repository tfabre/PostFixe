#include <stdbool.h>
#include <assert.h>

#include "linked_list_chunk.h"
#include "smem.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Structure used to store list of \link chunk\endlink 's.
 *
 * Please note that \c linked_list_chunk does not make any copy of \c chunk's.
 *
 * The list is bidirectional and can be accessed from both end.
 *
 * The structure is 2-level:
 * \li \c link's are used to provide the bidirectional links(with pointers)
 *     and points to \c chunk
 * \li the \c main structure has pointers to first and last \c link's.
 *
 * The empty list has the pointers of its \c main structure at \c NULL.
 *
 * \note Cela ressemble au TDM2(sans être circulaire).
 *
 * assert is enforced.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

/*! 
 * \c linked_list_chunk is a pointer to a hidden structure(\c main structure). 
 */

typedef struct link_chunk_struct* link_chunk;

struct link_chunk_struct
{
    link_chunk prev;
    link_chunk next;
    chunk value;
};

struct linked_list_chunk_struct
{
    link_chunk head;
    link_chunk end;
    unsigned int size;
};

static link_chunk link_chunk_create(void)
{
    link_chunk ret = (link_chunk)smalloc(sizeof(struct link_chunk_struct));
    ret->prev = NULL;
    ret->next = NULL;
    ret->value = NULL;
    return ret;
}

/*!
 * Generate an empty \c linked_list_chunk
 *
 * \return an empty \c linked_list_chunk
 */
linked_list_chunk linked_list_chunk_create(void) 
{
    linked_list_chunk ret = (linked_list_chunk)smalloc(
        sizeof(struct linked_list_chunk_struct));
    ret->head  = NULL;
    ret->end  = NULL;
    ret->size = 0;

    return ret;
}

/*!
 * Destroy the whole structure and the stored values.
 *
 * \param llc \c linked_list_chunk to destroy
 * \pre \c llc is valid(assert-ed)
 */
void linked_list_chunk_destroy(linked_list_chunk llc) 
{
    assert(llc != NULL);

    link_chunk next = llc->head;

    while (NULL != next)
    {
        link_chunk tmp = next->next;
        chunk_destroy(next->value);
        sfree(next);
        next = tmp;
    }
    sfree(llc);
}

/*!
 * To know whether a \c linked_list_chunk is empty.
 *
 * \param llc \c linked_list_chunk to test
 * \pre \c llc is valid(assert-ed)
 * \return true iff \c llc is empty
 */
bool linked_list_chunk_is_empty(linked_list_chunk llc) 
{
    assert(llc != NULL);

    return llc->size == 0;
}

/*!
 * To print a \c linked_list_chunk.
 * Each chunk is printed on a separate line with \c chunk_print.
 *
 * \param llc \c linked_list_chunk to print
 * \param f stream to print to
 * \pre \c llc is valid(assert-ed)
 * \pre \c f is not \c NULL(assert-ed)
 */
void linked_list_chunk_print(linked_list_chunk llc, FILE* f) 
{
    assert(NULL != llc);
    assert(NULL != f);

    link_chunk current = llc->head;

    while (NULL != current)
    {
        chunk_print(current->value, f);
        current = current->next;
        fputc('\n', f);
    }
}

/*!
 * Add a chunk at the beginning of the \c linked_list_chunk.
 *
 * \param llc \c linked_list_chunk to add to
 * \param ch \c chunk to add
 * \pre \c llc is valid(assert-ed)
 * \pre \c ch is not \c NULL(assert-ed)
 */
void linked_list_chunk_add_front(linked_list_chunk llc, chunk ch) 
{
    assert(NULL != llc);
    assert(NULL != ch);

    link_chunk newlink = link_chunk_create();
    newlink->value = ch;

    if (NULL == llc->head)
    {
        llc->head = newlink;
        llc->end  = newlink;
    }
    else
    {
        newlink->prev = NULL;
        newlink->next = llc->head;
        llc->head->prev = newlink;
        llc->head = newlink;
    }
    llc->size++;
}

/*!
 * Add a \c chunk at the end of the \c linked_list_chunk.
 *
 * \param llc \c linked_list_chunk to add to
 * \param ch \c chunk to add
 * \pre \c llc is valid(assert-ed)
 * \pre \c ch is not \c NULL(assert-ed)
 */
void linked_list_chunk_add_back(linked_list_chunk llc, chunk ch) 
{
    assert(NULL != llc);
    assert(NULL != ch);

    link_chunk newlink = link_chunk_create();
    newlink->value = ch;

    if (llc->head == NULL)
    {
        llc->head = newlink;
        llc->end  = newlink;
    }
    else
    {
        newlink->next = NULL;
        newlink->prev = llc->end;
        llc->end->next = newlink;
        llc->end = newlink;
    }

    llc->size++;
}

/*!
 * Return the \c chunk at the beginning of the \c linked_list_chunk.
 * The \c chunk is removed from the \c linked_list_chunk.
 *
 * \param llc \c linked_list_chunk to pop from
 * \return The removed \c chunk at the beginning or \c NULL if
 *         linked_list_chunk empty
 */
chunk linked_list_chunk_pop_front(linked_list_chunk llc) 
{
    assert(NULL != llc);

    link_chunk first = llc->head;
    chunk pop = first->value;

    if (llc->head == llc->end)
    {
        llc->head = NULL;
        llc->end  = NULL;
    } 
    else
    {
        llc->head = llc->head->next;
        llc->head->prev = NULL;
        first->next = NULL;
    }

    sfree(first);
    llc->size--;

    return pop;
}

/*!
 * Add a \b copy of the \c k first \c chunk at the beginning of the
 * \c linked_list_chunk to it-self.
 * If there is less than \c k \c chunk then no copy is made.
 *
 * For \c k, the following \c linked_list_chunk
 * \verbatim [front]                                                                         ch0 ch_1 ch_2 ...    ch_k-2 ch_k-1     ch_k ch_k+1 ch_k+2 \endverbatim 
 * is transformed into
 * \verbatim [front]    ch0 ch_1 ch_2 ...    ch_k-2 ch_k-1     ch0 ch_1 ch_2 ...    ch_k-2 ch_k-1     ch_k ch_k+1 ch_k+2 \endverbatim 
 *
 * \param llc \c linked_list_chunk to add to
 * \param k size of the prefix to copy in front
 * \pre \c llc is valid(assert-ed)
 * \return false if there where less than k element. In such a case, no copy
 *         is made.
 */
bool linked_list_chunk_add_self_copy_front(linked_list_chunk llc,
                                           unsigned int k)
{
    assert(llc != NULL);

    if (llc->size < k)
        return false;
    
    link_chunk current = llc->head;
    for (unsigned int i = 1; i < k; i++)
        current = current->next;

    for (unsigned int i = 0; i < k; i++)
    {
        linked_list_chunk_add_front(llc, chunk_copy(current->value));
        current = current->prev;
    }

    return true;
}

/*!
 * Generate a copy of the \c linked_list_chunk.
 *
 * \param llc \c linked_list_chunk to copy
 * \pre \c llc is valid(assert-ed)
 */
linked_list_chunk linked_list_chunk_copy(linked_list_chunk llc) 
{
    assert(NULL != llc);

    linked_list_chunk llc_ret = linked_list_chunk_create();

    link_chunk current = llc->head;
    while (NULL != current)
    {
        linked_list_chunk_add_back(llc_ret, chunk_copy(current->value));
        current = current->next;
    }

    return llc_ret;
}

chunk linked_list_chunk_get_front(linked_list_chunk llc)
{
    assert(NULL != llc);

    if (NULL == llc->head)
        return NULL;

    return llc->head->value;
}
