#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_pop.h"
#include "macro_operator_c.h"
#include "value_error.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c pop: remove the top \c value from the stack.
 *
 * The \c value is destroyed.
 * Nothing else is modified.
 * 
 * If the stack is empty, a \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static basic_type operator_pop_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk head = linked_list_chunk_get_front(context->stack);
    if (NULL == head)
        return basic_type_error;
    linked_list_chunk_pop_front(context->stack);
    chunk_destroy(head);
    return basic_type_void;
}

OPERATOR_BASIC_FULL(pop, pop)
