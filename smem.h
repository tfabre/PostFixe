/*!
** \file
** \brief Provide "secure" memory functions.
**
** Assert is enforced.
**
** \author Thomas Fabre
** \version 1
** \date 2015
** \copyright GNU Public License.
*/

#ifndef SMEM_H_
#define SMEM_H_

#include <stdlib.h>

/*!
** \brief "Secures" the free function.
**
** \param elem The pointeur to free.
*/
#define sfree(elem) do                          \
    {                                           \
        if (NULL != elem)                       \
        {                                       \
            free(elem);                         \
            elem = NULL;                        \
        }                                       \
    } while(0)

/*!
** \brief "Secures" the free function and don't assigne NULL to the pointer.
**
** \param elem A const pointer to be free.
*/
#define const_sfree(elem) do                    \
    {                                           \
        if (NULL != (elem))                     \
            free(elem);                         \
                                                \
    } while(0)

/*!
** \brief "Secures" the malloc function.
**
** \param size The size to be allocated.
** \return A pointer to the newly allocated memory.
*/
void* smalloc(const size_t size);

/*!
** \brief "Secures" the cmalloc function.
**
** \param num Number of elements to allocate.
** \param size Size of each element.
** \return A pointer to the newly allocated memory.
*/
void* scalloc(const size_t num, const size_t size);

char* rescale(char** src, const size_t size);

#endif // SMEM_H_
