#include <stdio.h>
#include <assert.h>

#include "value.h"
#include "operator.h"

#include "interpreter.h"
#include "read_chunk_io.h"
#include "smem.h"
#include "value_block.h"
#include "value_error.h"
#include "operator_label.h"
#include "operator_print_stack.h"
#include "basic_type.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

static interpretation_context interpretation_context_create(FILE* f, bool trace)
{
    interpretation_context context = (interpretation_context)smalloc(
        sizeof(interpretation_context_struct));

    context->program_input_stream = f;
    context->stack = linked_list_chunk_create();
    context->dic = dictionary_create();
    context->do_trace = trace;

    return context;
}

static void interpretation_context_destroy(interpretation_context context)
{
    fclose(context->program_input_stream);
    linked_list_chunk_destroy(context->stack);
    dictionary_destroy(context->dic);
    sfree(context);
}

static void print_stack(chunk c, interpretation_context ic)
{
    if (ic->do_trace)
        operator_evaluate(operator_print_stack_create(), ic);
}

static void print_read(chunk c, interpretation_context ic)
{
    if (ic->do_trace)
    {
        fprintf(stdout, "==**== reading: ");
        chunk_print(c, stdout);
        if (chunk_is_operator(c))
            puts(" (operator)");
        else
            puts(" (value)");
    }
}

static void print_label(chunk current, interpretation_context context)
{
    if (context->do_trace)
    {
        fputs("DECLANCHEMENT DE ", stdout);
        chunk_print(current, stdout);
        puts("");
        if (!value_is_block(linked_list_chunk_get_front(context->stack)))
        {
            chunk cur = linked_list_chunk_get_front(context->stack);
            print_read(cur, context);
            print_stack(cur, context);
        }
    }
}
void interprete_block_reading(const chunk ch, interpretation_context ic)
{
    chunk current = ch;
    llc stacks = linked_list_chunk_create();
    llc current_stack = value_block_get_list(current);
    linked_list_chunk_add_front(stacks, (chunk)current_stack);

    while (NULL != (llc)linked_list_chunk_get_front(stacks))
    {
        if (NULL == (current = read_chunk_io(ic->program_input_stream)))
            continue;

        if (value_is_end_block(current))
        {
            chunk_destroy(current);
            linked_list_chunk_pop_front(stacks);
            current_stack = (llc)linked_list_chunk_get_front(stacks);
            continue;
        }
        linked_list_chunk_add_back(current_stack, current);
        if (value_is_block(current))
        {
            current_stack = value_block_get_list(current);
            linked_list_chunk_add_front(stacks, (chunk)current_stack);
        }
    }
    linked_list_chunk_destroy(stacks);
}

void interprete_chunk(chunk current, interpretation_context context)
{
    bool interactive = context->program_input_stream == stdin;
    if (!value_is_error(current) && !chunk_is_operator(current))
        linked_list_chunk_add_front(context->stack, current);

    if (value_is_error(current) && interactive)
    {
        chunk_print(current, stdout);
        puts("");
    }

    if (value_is_block(current))
        interprete_block_reading(current, context);

    print_read(current, context);

    if (chunk_is_operator(current))
    {
        
        if (operator_evaluate(current, context).type == basic_type_error.type)
            puts("interpretation error");

        else if (operator_is_label(current))
        {
            print_label(current, context);
            chunk_destroy(current);

            if (!value_is_block(linked_list_chunk_get_front(context->stack)))
                current = linked_list_chunk_get_front(context->stack);
            else
            {
                chunk block = linked_list_chunk_pop_front(context->stack);
                linked_list_chunk ch = value_block_get_list(block);
                interprete_chunk_list(ch, context);
                chunk_destroy(block);
            }
        }
    }
    print_stack(current, context);
}

void interprete_chunk_list(linked_list_chunk ch, interpretation_context ic) 
{
    chunk c = NULL;
    do
    {
        c = linked_list_chunk_get_front(ch);
        if (NULL == c)
            break;
        
        linked_list_chunk_pop_front(ch);
        if (value_is_block(c))
        {
            print_read(c, ic);            
            linked_list_chunk_add_front(ic->stack, c);
            print_stack(c, ic);
        }
        else
            interprete_chunk(c, ic);
    } while (NULL != c);
}

/*!
** \brief Indicate if the interpretation is finished (EOF).
**
** \param stream Stream where the program comes from.
** \return true if the file is finished to be interpreted, false otherwise.
*/
static bool is_interpretation_finished(FILE* stream)
{
    char current = fgetc(stream);
    if (EOF == current)
        return true;

    ungetc(current, stream);
    return false;
}

void interprete(FILE* f, bool do_trace) 
{
    interpretation_context context = interpretation_context_create(f, do_trace);
    bool interactive = context->program_input_stream == stdin;
    chunk current;
    if (interactive)
        fputs("> ", stdout);

    while (!is_interpretation_finished(context->program_input_stream))
    {
        if (NULL == (current = read_chunk_io(context->program_input_stream)))
        {
            if (interactive)
                fputs("> ", stdout);
            continue;
        }

        interprete_chunk(current, context);
    }

    if (do_trace)
    {
        fprintf(stdout, "======= dictionnary ==============\n");
        dictionary_print(context->dic, stdout);
    }
    fprintf(stdout, "======== final stack =============\n");
    linked_list_chunk_print(context->stack, stdout);
    interpretation_context_destroy(context);
}
