#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_def.h"
#include "macro_operator_c.h"

#include "value_protected_label.h"
#include "value_error.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c def: add an entry into the \c dictionary.
 *
 * The entry is composed of a protected label on the top of the stack and the
 * value under it.
 * Both values are removed from the stack.
 * Nothing else is modified.
 * 
 * If the stack is not deep enough or the first one is not a
 * \c value_protected_label, a \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static basic_type operator_def_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk label = linked_list_chunk_get_front(context->stack);

    if (NULL == label || !value_is_protected_label(label))
        return basic_type_error;
    linked_list_chunk_pop_front(context->stack);
    chunk val = linked_list_chunk_get_front(context->stack);
    if (NULL == val)
    {
        linked_list_chunk_add_front(context->stack, label);
        return basic_type_error;
    }
    linked_list_chunk_pop_front(context->stack);

    dictionary_set(context->dic, *(sstring*)label->state, val);
    chunk_destroy(label);
    chunk_destroy(val);

    return basic_type_void;
}

OPERATOR_BASIC_FULL(def, def)
