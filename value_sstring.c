#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "value_sstring.h"
#include "macro_value_c.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file 
 * \brief \c value to hold a \c sstring.
 *
 * \c value_sstring are read and printed with \c '"' delimiters like
 * <tt>"ed fe"</tt>.
 * On input, use \c "\"" for a \c '"', \c '\n' for a newline, \c '\t' for a
 * tabulation and \c '\\' for a backspace char.
 * Protective \c '\\' are not restored on printing(i.e. follows the \c print of
 * \link sstring\endlink)
 *
 * There is \em no \em limit to the length of a string.
 *
 * assert is enforced.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static void _value_sstring_print(const sstring val, FILE* f_out)
{
    fputc('"', f_out);
    sstring_print(val, f_out);
    fputc('"', f_out);
}

VALUE_REACTIONS_PTR(sstring, sstring, pointer)

VALUE_DECLARE(sstring, sstring) 
