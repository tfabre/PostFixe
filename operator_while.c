#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_while.h"
#include "macro_operator_c.h"
#include "linked_list_chunk.h"
#include "operator_label.h"
#include "value_boolean.h"
#include "value_block.h"
#include "value_protected_label.h"

#include "value_error.h"

#include "interpreter.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c while: as long as the fist \c value on the top evaluates
 *        to true evaluate the previous argument. 
 *
 * The first two \c value's are removed and used each time on the remaining of
 * the stack.
 * At the end, they are destroyed.
 *
 * The two values on top must be \c values_block or \c values_protected_label.
 * Any \c values_protected_label is evaluated as the corresponding
 * \c operator_label.
 * They are re-evaluated each time.
 *
 * The first one should leave a \c value_boolean on top of the stack(any other
 * modification due to the evaluation remains).
 * \li If the value is true, then the other \c value is evaluated and then the
 *     process is restarted.
 * \li If the value is false, then the evaluation is finished.
 * 
 * If the stack is not deep enough or a wrong kind of \c value is found, then a
 * \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static void evaluate_loop(const chunk op, interpretation_context context)
{
    if (value_is_block(op))
        interprete_chunk_list(value_block_get_list(op), context);

    else if (value_is_protected_label(op))
    {
        sstring label_name = sstring_copy(*(sstring*)op->state);
        interprete_chunk(operator_label_create(label_name), context);
    }
}

static basic_type operator_while_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk cond = linked_list_chunk_get_front(context->stack);

    if (NULL == cond || (!value_is_protected_label(cond) &&
                         !value_is_block(cond)))
        return basic_type_error;

    linked_list_chunk_pop_front(context->stack);
    chunk op = linked_list_chunk_get_front(context->stack);
    if (NULL == op || (!value_is_protected_label(op) && !value_is_block(op)))
    {
        linked_list_chunk_add_front(context->stack, cond);
        return basic_type_error;
    }
    linked_list_chunk_pop_front(context->stack);

    bool ok = false;
    do
    {
        chunk cond_cpy = chunk_copy(cond);
        evaluate_loop(cond_cpy, context);
        chunk_destroy(cond_cpy);
        chunk chunk_ok = linked_list_chunk_get_front(context->stack);
        if (NULL == chunk_ok || !value_is_boolean(chunk_ok))
            return basic_type_error;
        linked_list_chunk_pop_front(context->stack);
        ok = *(bool*)chunk_ok->state;
        chunk_destroy(chunk_ok);
        if (!ok)
            break;
        chunk loop_cpy = chunk_copy(op);
        evaluate_loop(loop_cpy, context);
        chunk_destroy(loop_cpy);
    } while (ok);

    chunk_destroy(cond);
    chunk_destroy(op);
    return basic_type_void;
}

OPERATOR_BASIC_FULL(while, while)
