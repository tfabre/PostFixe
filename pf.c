#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#include "interpreter.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief THE MAIN FILE!
 *
 * Arguments:
 * \li \c -h help message(and exit)
 * \li \c -t to trace(can be turned off by operator \c stop_trace)
 * \li \c file.pf file to interprete(otherwise it is stdin)
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \year 2015
 */

/*!
 * print help message and exit.
 */
static void help_message(const char* const prog_name)
{
    puts("USAGE:");
    printf(" %s -h\n\tDisplay this message and exit\n", prog_name);
    printf(" %s [OPTIONS] [FILE]\n\tRun the pf interpreter on [FILE](standard "
           "input if void)\n", prog_name);
    puts("OPTIONS:");
    puts(" -t to trace the execution");
    exit(0);
}

/*!
 * THE MAIN FUNCTION
 */
int main(const int argc, const char* const argv[])
{
    bool trace = false;
    FILE* input_stream = stdin;

    if (3 < argc)
        help_message(argv[0]);

    for (int i = 1; i < argc; ++i)
    {
        if (0 == strcmp(argv[i], "-h"))
            help_message(argv[0]);

        else if (0 == strcmp(argv[i], "-t"))
            trace = true;

        else if (NULL == (input_stream = fopen(argv[i], "r")))
        {
            fprintf(stderr, "Error: %s can't be opened\n", argv[i]);
            return 0;
        }
    }

    interprete(input_stream, trace);
    return 0;
}
