#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>

#include "smem.h"
#include "value_int.h"
#include "macro_value_c.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file 
 * \brief \c value to hold an \c int.
 * 
 * Integers are read and printed in the usual way:
 * \li '-' sign if negative(nothing for positive) followed by
 * \li sequence of digits.
 *
 * assert is enforced.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static void _value_int_print(const long long int val, FILE* f_out)
{
    fprintf(f_out, "%lli", val);
}

VALUE_REACTIONS(int, long long int, long_long_int)

VALUE_DECLARE(int, long long int) 
