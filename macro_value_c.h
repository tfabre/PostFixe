#ifndef __MACRO_VALUE_C_H
#define __MACRO_VALUE_C_H

#include "basic_type.h"
#include "smem.h"

#undef VALUE_DECLARE

#define VALUE_GET_VALUE(type_name, type_C, type_getter)                 \
    static basic_type value_ ## type_name ## _get_value(const chunk ch, \
                                                        va_list va)     \
    {                                                                   \
        return basic_type_ ## type_getter(*(type_C*)ch->state);         \
    }

#define VALUE_DESTROY(type_name)                                        \
    static basic_type value_ ## type_name ## _destroy(const chunk ch,   \
                                                      va_list va)       \
    {                                                                   \
        if (NULL != ch)                                                 \
        {                                                               \
            sfree(ch->state);                                           \
            const_sfree(ch);                                            \
        }                                                               \
        return basic_type_void;                                         \
    }

#define VALUE_DESTROY_PTR(type_name, type_C)                            \
    static basic_type value_ ## type_name ## _destroy(const chunk ch,   \
                                                      va_list va)       \
    {                                                                   \
        if (NULL != ch)                                                 \
        {                                                               \
            type_C ## _destroy(*(type_C*)ch->state);                    \
            sfree(ch->state);                                           \
            const_sfree(ch);                                            \
        }                                                               \
        return basic_type_void;                                         \
    }

/*! \warning this function require a function _value_<type_name>_print(void*) */
#define VALUE_PRINT(type_name, type_C)                              \
    static basic_type value_ ## type_name ## _print(const chunk ch, \
                                                    va_list va)     \
    {                                                               \
        FILE* f_out = va_arg(va, FILE*);                            \
        _value_ ## type_name ## _print(*(type_C*)ch->state, f_out); \
        return basic_type_void;                                     \
    }

#define VALUE_COPY(type_name, type_C)                               \
    static basic_type value_ ## type_name ## _copy(const chunk ch,  \
                                                   va_list va)      \
    {                                                               \
        return basic_type_pointer(                                  \
            value_ ## type_name ## _create(*(type_C*)ch->state));   \
    }

#define VALUE_COPY_PTR(type_name, type_C)                           \
    static basic_type value_ ## type_name ## _copy(const chunk ch,  \
                                                   va_list va)      \
    {                                                               \
        return basic_type_pointer(                                  \
            value_ ## type_name ## _create(                         \
                (void*)type_C ## _copy(*(type_C*)ch->state)));      \
    }

#define REACTIONS(type_name)                                            \
    static const message_action value_ ## type_name ## _reactions[] =   \
    {                                                                   \
        MESSAGE_ACTION__BASIC_VALUE(type_name),                         \
        {NULL, NULL}                                                    \
    };

#define VALUE_REACTIONS(type_name, type_C, type_getter) \
    VALUE_GET_VALUE(type_name, type_C, type_getter)     \
    VALUE_DESTROY(type_name)                            \
    VALUE_PRINT(type_name, type_C)                      \
    VALUE_COPY(type_name, type_C)                       \
    REACTIONS(type_name)

#define VALUE_REACTIONS_PTR(type_name, type_C, type_getter) \
    VALUE_GET_VALUE(type_name, type_C, type_getter)         \
    VALUE_DESTROY_PTR(type_name, type_C)                    \
    VALUE_PRINT(type_name, type_C)                          \
    VALUE_COPY_PTR(type_name, type_C)                       \
    REACTIONS(type_name)

#define VALUE_DECLARE(type_name, type_C)                       \
    chunk value_ ## type_name ## _create(const type_C val)     \
    {                                                          \
        chunk ret = (chunk)smalloc(sizeof(chunk_struct));      \
                                                               \
        ret->state = smalloc(sizeof(type_C));                  \
        *(type_C*)(ret->state) = val;                          \
        ret->reactions = value_ ## type_name ## _reactions;    \
                                                               \
        return ret;                                            \
    }                                                          \
                                                               \
    VALUE_IS_FULL(type_name)

#define VALUE_IS_FULL(type_name)                                    \
    bool value_is_ ## type_name(const chunk ch)                     \
    {                                                               \
        assert(NULL != ch);                                         \
        return value_ ## type_name ##_reactions == ch->reactions;   \
    }

#endif
