#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "smem.h"

#include "read_chunk_io.h"

#include "value_boolean.h"
#include "value_sstring.h"
#include "value_double.h"
#include "value_int.h"
#include "value_block.h"
#include "value_protected_label.h"
#include "value_error.h"

#include "operator_addition.h"
#include "operator_division.h"
#include "operator_multiplication.h"
#include "operator_subtraction.h"
#include "operator_remainder.h"

#include "operator_less.h"
#include "operator_less_equal.h"

#include "operator_equal.h"
#include "operator_different.h"

#include "operator_and.h"
#include "operator_or.h"
#include "operator_not.h"

#include "operator_label.h"

#include "operator_creator_list.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Read \c chunk from a stream.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

/*!
 * Maximal length of a token or label.
 * Larger length labels or keywords are not allowed.
 */
#define TOKEN_KEYWORD_MAX_LENGTH 50

/*!
 * Read a token from a stream.
 *
 * The first(non-space) character is read and depending on it, only some
 * \c chunk are possible.
 * More characters are read until:
 * \li chunk is fully read, and
 * \li there is no ambiguity (\c"!" from \c"!=", minus operation from sign of
 * number…).
 * If one(or more) \c char that could belong to another \c chunk are read to
 * settle ambiguity, they are put back(with \c ungetc).
 *
 * Label(protected or not) are distinguished from keywords by using the table
 * in \c operator_creator_list.
 *
 * Static functions are used to handle separately numbers(int or float), label
 * or keyword, sstring, block…
 *
 * Following spaces are discarded.
 *
 * If the OEF is reached before anything is read, then a \c value_error with
 * \c VALUE_ERROR_IO_EOF is returned.
 *
 * If the input cannot form a legal \c chunk, then a corresponding
 * \c value_error is returned.
 *
 * \param f \c stream to read from
 * \pre \c f is not \c NULL(assert-ed)
 * \return chunk read(should be a \c value or an \c operator) or a
 *         \c value_error if reading fail.
 */

/*!
** \brief Skip between tokens from stream.
**
** \param stream The program stream.
** \return true if the file isn't completly read, false otherwise.
*/
static bool skip_blank(FILE* stream)
{
    char current;
    for (current = fgetc(stream); !feof(stream); current = fgetc(stream))
        if ('\n' == current || (!iscntrl(current) && !isblank(current)))
            break;

    ungetc(current, stream);
    return true;
}

#define STOP_GETTING_TOKEN(current, stream, car)    \
    if (car == current)                             \
    {                                               \
        ungetc(current, stream);                    \
        break;                                      \
    }

/*!
** \brief Get token from stream.
**
** \param token String to write the current token in stream.
** \param stream Stream to extract tokens.
** \return The token's size
*/
static int get_token(char* token, FILE* stream)
{
    skip_blank(stream);
    char current = fgetc(stream);
    size_t index = 0;

    if ('}' == current || '"' == current || '\n' == current)
    {
        token[index++] = current;
        return index;
    }

    memset(token, 0, TOKEN_KEYWORD_MAX_LENGTH);

    for (; !feof(stream) && index < TOKEN_KEYWORD_MAX_LENGTH;
         current = fgetc(stream))
    {
        STOP_GETTING_TOKEN(current, stream, '\n');
        if (iscntrl(current) || isblank(current))
            break;

        STOP_GETTING_TOKEN(current, stream, '}');
        token[index++] = current;

        if ('{' == current)
            break;
    }
    return index;
}

/*!
** \brief Finish reading a string fromo a stream.
**
** \param token Beginning of the string token.
** \param token_size Size of the token given previously.
** \param stream Stream where the string comes from
** \return String token length
*/
static int get_string_token(char** token, size_t token_size, FILE* stream)
{
    size_t tmp_token_size = TOKEN_KEYWORD_MAX_LENGTH;
    size_t tmp_token_len = 0;
    char* tmp_token = (char*)scalloc(sizeof(char), TOKEN_KEYWORD_MAX_LENGTH +1);
    char last_current = fgetc(stream);

    for (char current = last_current; '\n' != current || !feof(stream);
         current = fgetc(stream))
    {
        tmp_token[tmp_token_len++] = current;

        if (tmp_token_size < tmp_token_len +1)
        {
            tmp_token_size += TOKEN_KEYWORD_MAX_LENGTH;
            rescale(&tmp_token, tmp_token_size +1);
        }

        if ('"' == current && '\\' != last_current)
            break;
        last_current = current;
    }
    rescale(token, token_size + tmp_token_len +1);
    strcat(*token, tmp_token);
    sfree(tmp_token);

    return token_size + tmp_token_len;
}

/*!
** \brief Transform a token into a numeric chunk.
**
** \param token Token representation of number.
** \return The numeric chunk obtained from token.
*/
static chunk get_num(const char* token)
{
    chunk ret = NULL;

    if (NULL != strchr(token, '.'))
        ret = value_double_create(atof(token));
    else
        ret = value_int_create(atoll(token));

    return ret;
}

/*!
** \brief Transform a token into a value_sstring chunk.
**
** \param token Token to transform.
** \param token_size Size of the token previously given.
** \param stream Stream where the token comes from
** \return The value_sstring chunk.
*/
static chunk get_string(char** token, size_t token_size, FILE* stream)
{
    token_size = get_string_token(token, token_size, stream);

    (*token)[token_size -1] = '\0';
    return value_sstring_create(sstring_create_string(*token +1));
}

/*!
** \brief Transform a token into a protected label.
**
** \param token Token to transform.
** \return The protected_label chunk obtained from the token.
*/
static chunk get_protected_label(char* token)
{
    return value_protected_label_create(sstring_create_string(token +1));
}

/*!
** \brief Transform a boolean into a chunk.
**
** \param b Boolean to transform.
** \return The boolean chunk.
*/
static chunk get_bool(bool b)
{
    return value_boolean_create(b);
}

/*!
** \brief Return a new block.
**
** \return A new block.
*/
static chunk get_block()
{
    return value_block_create(linked_list_chunk_create());
}

/*!
** \brief Return a chunk that represent a block ending.
**
** \return end_block chunk.
*/
static chunk get_end_block()
{
    return value_end_block_create(linked_list_chunk_create());
}

/*!
** \brief Return operator corresponding to token given in parameter.
**
** \param op Token which contains symbole of operation.
** \return Operator if it has been found.
*/
static chunk get_operator(const char* op)
{
    assert(NULL != op);
    chunk ret = NULL; /* value_error_create(VALUE_ERROR_ILLEGAL_OPERAND) */;

    for (int i = 0; NULL != operator_creator_list[i].keyword &&
             NULL != operator_creator_list[i].create_operator; ++i)
        if (0 == strcmp(op, operator_creator_list[i].keyword))
        {
            chunk_destroy(ret);
            ret = (*operator_creator_list[i].create_operator)();
        }

    if (NULL == ret)
        ret = operator_label_create(sstring_create_string(op));

    return ret;
}

chunk read_chunk_io(FILE* f) 
{
    assert(NULL != f);
    chunk ret = NULL;
    char* token = (char*)scalloc(sizeof(char), TOKEN_KEYWORD_MAX_LENGTH +1);

    size_t token_size = get_token(token, f);

    if (('-' == token[0] && 1 < strlen(token) && isdigit(token[1])) ||
        isdigit(token[0]))
        ret = get_num(token);

    else if ('"' == token[0])
        ret = get_string(&token, token_size, f);

    else if ('\\' == token[0])
        ret = get_protected_label(token);

    else if (0 == strcmp(token, "true"))
        ret = get_bool(true);

    else if (0 == strcmp(token, "false"))
        ret = get_bool(false);

    else if ('{' == token[0])
        ret = get_block();

    else if ('}' == token[0])
        ret = get_end_block();

    else if ('\n' != token[0] && NULL != token && 0 < strlen(token))
        ret = get_operator(token);

    sfree(token);
    return ret;
}
