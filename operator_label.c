#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_label.h"
#include "macro_operator_c.h"
#include "value_block.h"
#include "value_error.h"
#include "smem.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c label: when evaluated it is replaced by the corresponding
 * \c value from the \c dictionary.
 *
 * If the associated \c value is a \c value_block, then the \c chunk's are
 * processed in order.
 * Otherwise, it is processed as usually.
 *
 * Label can be created even if the entry is not defined in the dictionary.
 * It should be defined when evaluated.
 * 
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static basic_type operator_label_destroy(const chunk ch, va_list va)
{
    if (NULL != ch)
    {
        sstring_destroy((sstring)ch->state);
        const_sfree(ch);
    }
    return basic_type_void;
}

static basic_type operator_label_copy(const chunk ch, va_list va)
{
    return basic_type_pointer(operator_label_create(
                                  sstring_copy((sstring)ch->state)));
}

static basic_type operator_label_print(const chunk ch, va_list va)
{
    FILE* stream = va_arg(va, FILE*);
    sstring_print((sstring)ch->state, stream);
    return basic_type_void;
}

static basic_type operator_label_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk entry = dictionary_get_copy(context->dic, (sstring)ch->state);
    if (NULL == entry)
    {
        operator_label_destroy(ch, NULL);
        return basic_type_error;
    }

    linked_list_chunk_add_front(context->stack, entry);

    return basic_type_void;
}

static const message_action operator_label_reactions[] =
{
    MESSAGE_ACTION__BASIC_OPERATOR(label),
    { NULL, NULL }
};

/*!
 * Create an \c operator_label.
 *
 * \param ss non empty string
 * \pre \c ss in not \ NULL and non-empty(assert-ed)
 * \return a newly created \c operator_label
 */
chunk operator_label_create(sstring ss) 
{
    assert(NULL != ss);
    assert(!sstring_is_empty(ss));

    chunk ret = (chunk)smalloc(sizeof(chunk_struct));
    ret->state = (void*)ss;
    ret->reactions = operator_label_reactions;

    return ret;
}

/*!
 * Test whether a chunk is an operator_label
 *
 * \param ch cunk to test
 * \return true iff it is an operator_label
 */
bool operator_is_label(const chunk ch)
{
    return ch->reactions == operator_label_reactions;
}
