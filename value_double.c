#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "value_double.h"
#include "macro_value_c.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION!

/*!
 * \file 
 * \brief \c value used to hold a \c double.
 *
 * For I/O these are just numbers with decimal point like 78.0 0.75 -568.58.
 * No exponential form is supported.
 *
 * assert is enforced.
 *
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

/*
** NOTE:
** According to expected results the conversion specifier is for float
** variables.
**
** Otherwise the specifier should be "%Lg" of "%LG" because we manipulate
** double.
*/
static void _value_double_print(const long double val, FILE* f_out)
{
    fprintf(f_out, "%Lf", val);
}

VALUE_REACTIONS(double, long double, long_double)

VALUE_DECLARE(double, long double) 
