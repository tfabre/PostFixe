#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "operator_if_else.h"
#include "operator_label.h"
#include "macro_operator_c.h"

#include "value_protected_label.h"
#include "value_boolean.h"
#include "value_block.h"
#include "value_error.h"

#undef NDEBUG     // FORCE ASSERT ACTIVATION

/*!
 * \file
 * \brief Operator \c if_else: when evaluated depending the boolean on top of
 *        the stack activate next or following value on the stack.
 *
 * The \c value on top    must be a \c value_boolean and the two \c value's
 * under mus be \c value_block or \c value_protected_label.
 * If the evaluated \c value is a \c value_block, the chunks evaluated in order.
 * If the evaluated \c value is a \c value_protected_label, then it as
 * evaluated as the corresponding \c operator_label.
 * All three \c value's are destroyed.
 * 
 * If the stack is not deep enough or a wrong kind of \c value is found, then a
 * \c basic_type_error is returned.
 *
 * assert is enforced.
 * 
 * \author Jérôme DURAND-LOSE
 * \author Romain DUVAL
 * \author Thomas FABRE
 * \author Jérôme HEBRAS
 * \version 1
 * \date 2015
 * \copyright GNU Public License.
 */

static void exec_instructions(chunk op, interpretation_context context)
{
    if (value_is_block(op))
        interprete_chunk_list(value_block_get_list(op), context);

    else if (value_is_protected_label(op))
    {
        sstring label_name = sstring_copy(*(sstring*)op->state);
        interprete_chunk(operator_label_create(label_name), context);
    }
    else
        interprete_chunk(op, context);
}

static basic_type operator_if_else_evaluate(const chunk ch, va_list va)
{
    interpretation_context context = va_arg(va, interpretation_context);
    chunk cond = linked_list_chunk_get_front(context->stack);

    if (NULL == cond || !value_is_boolean(cond))
        return basic_type_error;

    linked_list_chunk_pop_front(context->stack);
    chunk op_if = linked_list_chunk_get_front(context->stack);
    if (NULL == op_if)
    {
        linked_list_chunk_add_front(context->stack, cond);
        return basic_type_error;
    }
    linked_list_chunk_pop_front(context->stack);

    chunk op_else = linked_list_chunk_get_front(context->stack);
    if (NULL == op_else)
    {
        linked_list_chunk_add_front(context->stack, cond);
        linked_list_chunk_add_front(context->stack, op_if);
        return basic_type_error;
    }
    linked_list_chunk_pop_front(context->stack);

    if (*(bool*)cond->state)
        exec_instructions(op_if, context);
    else
        exec_instructions(op_else, context);

    chunk_destroy(cond);
    chunk_destroy(op_if);
    chunk_destroy(op_else);
    return basic_type_void;
}

OPERATOR_BASIC_FULL(if_else, if_else)
